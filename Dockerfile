FROM node:slim AS builder
WORKDIR /app
COPY package.json package-lock.json ./
RUN npm install
COPY . .
RUN npm run build

FROM node:alpine AS runner
COPY --from=builder app/dist /app/dist
WORKDIR /app
COPY package.json package-lock.json ./
CMD npm install --only=production && npm start